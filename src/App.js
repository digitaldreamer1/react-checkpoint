import React from "react";
import Name from "./Name";
import Price from "./Price";
import Description from "./Description";
import Image from "./Image";

const App = () => {
  const firstName = "VotrePrénom"; // Remplacez par votre prénom

  return (
    <div className="p-6">
      <div className="max-w-sm rounded overflow-hidden shadow-lg p-4">
        <Image />
        <div className="px-6 py-4">
          <Name />
          <Price />
          <Description />
        </div>
      </div>
      <div className="mt-4 text-center">
        {firstName ? `Bonjour, ${firstName}!` : "Bonjour, là !"}
        {firstName && (
          <img
            src="path_to_your_image"
            alt="profile"
            className="mt-4 w-24 h-24 rounded-full mx-auto"
          />
        )}
      </div>
    </div>
  );
};

export default App;
