import React from "react";
import Produict from "./Produict";

const Image = () => {
  return <img className="w-full" src={Produict.imageUrl} alt={Produict.name} />;
};

export default Image;
