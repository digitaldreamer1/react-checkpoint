import React from "react";
import Produict from "./Produict";

const Price = () => {
  return <p className="text-gray-700 text-base">${Produict.price}</p>;
};

export default Price;
